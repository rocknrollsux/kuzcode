package com.vasiliy.learning;
import java.util.Random;

public class TestOne extends Util {

	public static void main(String[] args) {
		int[][] test2DArray = generate2DArray(3, 3, 100);
		//int	minRowPos, minColPos, maxRowPos, maxColPos;
		
		print2DArray(test2DArray);
		
		int max = findMaxIn2DArray(test2DArray);
		int min = findMinIn2DArray(test2DArray);
		
		//test2DArray[minRowPos][minColPos] = max;
		//test2DArray[maxRowPos][maxColPos] = min;
		
	}
	
	public static int findMaxIn2DArray(int[][] test2DArray) {
		int max = test2DArray[0][0];
		
		for (int i = 0; i < test2DArray.length; i++) {
			for (int j = 0; j < test2DArray[i].length; j++) {
				if (max < test2DArray[i][j]) {
					max = test2DArray[i][j];
				}
			}
		}
		System.out.println(max + " is a maximum value in the given array");
		return max;
	}
	
	public static int findMinIn2DArray(int[][] test2DArray) {
		int min = test2DArray[0][0];
		
		for (int i = 0; i < test2DArray.length; i++) {
			for (int j = 0; j < test2DArray[i].length; j++) {
				if (min > test2DArray[i][j]) {
					min = test2DArray[i][j];
				}
			}
		}
		System.out.println(min + " is a minimum value in the given array");
		return min;
	}
	
	public static int[] findMaxMinAndSwap(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				int max = arr[i][0], min = arr[i][0], minPos, maxPos;
				if (max < arr[i][j]) {
					max = arr[i][j];
					maxPos = j;
				}
				if (min > arr[i][j]) {
					min = arr[i][j];
					minPos = j;	
				}
				arr[row][maxPos] = min;
				arr[row][minPos] = max;
			}
		}
	}
	
	/*public static void swapMinMax(int[][] array, int minRowPos, int maxRowPos, 
								int minColPos, int maxColPos, int min, int max) {
		
		array[minRowPos][minColPos] = max;
		array[maxRowPos][maxColPos] = min;
		
	}*/

}
