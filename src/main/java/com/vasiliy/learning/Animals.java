package com.vasiliy.learning;

public class Animals {
	String species;
	int age;
	String sound;
	int howManyLegs;
	
	public Animals(String species, String sound, int age, int howManyLegs) {
		this.species = species;
		this.age = age;
		this.sound = sound;
		this.howManyLegs = howManyLegs;
	}

	public void sleep() {
		System.out.println("Zzzzzzz....");
		System.out.println("Zzzzzzz....");
		System.out.println("Zzzzzzz....");
	}
	
	public void eat() {
		System.out.println("Hrm, hrm, hrm");
		System.out.println("Hrm, hrm, hrm");
	}
	
	public void speak() {
		System.out.println(sound);
	}
}
