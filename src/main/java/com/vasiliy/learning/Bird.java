package com.vasiliy.learning;

public class Bird extends Animals {
	boolean canFly;

	public Bird(String species, String sound, int age, int howManyLegs, boolean canFly) {
		super(species, sound, age, howManyLegs);
		this.canFly = canFly;
	}
	
	public void fly() {
		if (canFly) {
			System.out.println("I'm flying, and you're not, take that, loosers!");
			System.out.println("Shoo, shoo, shoo");
		}
		else {
			System.out.println("Sorry, I can't fly.");
			System.out.println("But I can walk, see?");
		}
	}

}
