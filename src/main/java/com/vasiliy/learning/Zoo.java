package com.vasiliy.learning;

public class Zoo {

	public static void main(String[] args) {
		
		Animals bear = new Animals("Polar Bear", "Grrrrrr", 5, 4);
		Animals fish = new Animals("Roach", "I'm a fish, what sounds do you expect, dummy?", 2, 0);
		Animals lion = new Animals("Lion The King", "Raaawwwr", 10, 4);
		Bird bird = new Bird("Any Bird", "Qwak, qwak, qwak", 1, 2, true);
		
		bear.sleep();
		
		fish.sleep();
		
		lion.eat();
		
		fish.speak();
		
		bird.fly();
		bird.speak();

	}

}
