package com.vasiliy.learning;

import java.util.Random;

public class Util {
	
	public static int[][] generate2DArray(int rowSize, int colSize, int maxVal) {
		int[][] array = new int[rowSize][colSize];
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
					array[i][j] = new Random().nextInt(maxVal);
			}
		}
		return array;
	}
	
	public static int[] generateArray(int size, int maxVal) {
		int[] array = new int[size];
		for (int i = 0; i < array.length; i++) {
			array[i] = new Random().nextInt(maxVal);
		}
		return array;
	}
	
	public static void printArray(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();
		System.out.println();
	}
	
	public static void print2DArray(int[][] array) {
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				System.out.print(array[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
	
}
