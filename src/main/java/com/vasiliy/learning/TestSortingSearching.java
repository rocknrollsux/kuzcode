package com.vasiliy.learning;

public class TestSortingSearching extends Util {

	public static void main(String[] args) {
		int[] testUnsortedArray = generateArray(10, 100);
		int[] testSortedArray = new int[testUnsortedArray.length];
		int[] testBinSearchArray = {1, 4, 5, 14, 16, 21, 54};
		
		// this loop copies values from unsorted array into wonnabe sorted array
		for (int i = 0; i < testUnsortedArray.length; i++) {
			        	testSortedArray[i] = testUnsortedArray[i];
		}
		
		printArray(testSortedArray);
		
		bubbleSort(testSortedArray);
		
		printArray(testSortedArray);
		
		System.out.println(binarySearch(testBinSearchArray, 54, 0, testBinSearchArray.length));
		
		
	}
	
	public static int[] bubbleSort(int[] arr) {
		for (int j = 0; j < arr.length-1; j++) {
			for (int i = 0; i < arr.length-1; i++) {
	      		if (arr[i] > arr[i+1]) {
	       			int temp = arr[i];
	       			arr[i] = arr[i+1];
	       			arr[i+1] = temp;
	       		}
	       	}
	    }
		return arr;
	}
	
	public static int binarySearch(int[] a, int value, int left, int right) {
		 
        // choose pivot index element as middle element
        int idx = (left+right)/2;
 
        // check if pivot index exists in the array
        if(idx>=a.length) {
            return -1;
        }
        int pivot = a[idx];
 
        if(left > right)
            return -1;
 
        if(value < pivot) {
            right = idx-1;
            return binarySearch(a, value, left, right);
        } else if(value > pivot) {
            left = idx+1;
            return binarySearch(a, value, left, right);
        } else {
            return pivot;
        }
    }

}
